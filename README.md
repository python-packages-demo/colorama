# [colorama](https://pypi.org/project/colorama/)

Cross-platform colored terminal text

* https://tracker.debian.org/pkg/python-colorama
---
* https://www.google.com/search?q=ANSI+color+Python+site:debian.org
* [*Print in terminal with colors?*](https://stackoverflow.com/questions/287871/print-in-terminal-with-colors)
* https://pypi.org/project/ansicolors/
* https://tracker.debian.org/pkg/python-blessed
* https://tracker.debian.org/pkg/colorclass
* https://tracker.debian.org/pkg/python-coloredlogs
* https://tracker.debian.org/pkg/python-couleur
* https://tracker.debian.org/pkg/python-xtermcolor
* https://tracker.debian.org/pkg/fabulous
* https://tracker.debian.org/pkg/python-daiquiri
---
* https://tracker.debian.org/pkg/prettytable
* https://tracker.debian.org/pkg/python-cement